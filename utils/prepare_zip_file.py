# -*- coding: utf-8 -*-
"""Prepare the zip file for the model
"""
import os
import shutil
import subprocess
from refutils import (base_to_turb, base_to_step, delete_dlls, clone_repo,
                      get_repo_info)


if __name__ == '__main__':
    # script inputs
    mod_dir = '../dtu-30mw-tri-rotor/'  # must end with slash
    zip_name = '../dtu-30mw-tri-rotor'
    basename = 'dtu_30mw_tri_rotor'
    dll_git_url = 'https://gitlab.windenergy.dtu.dk/OpenLAC/control-binary/control-win32.git'
    dll_branch = 'ref_models'
    dlls = ['dtu_we_controller_v2.3_v0.1.dev69.17400ca_17_11_2017_trirotor.dll',
            'generator_servo_v_2.2_v0.1.dev15.eddfec3_17_11_2017_trirotor.dll', 
            'mech_brake_v_2.2_v0.1.dev14.9e614a3_17_11_2017_trirotor.dll',
            'servo_with_limits_v2.2_v0.1.dev15.eddfec3_17_11_2017_trirotor.dll']
    dll_list = []
    for tag in ['center', 'left', 'right']:
        for dll in dlls:
            dll_list.append((dll, dll.replace('_trirotor', f'_{tag}')))

    # ======= make turbulent and step-wind files =======
    htc_base = mod_dir + f'htc/{basename}.htc'
    kwargs = dict(cut_in=3, cut_out=25, tstart=100, dt=40, wsp=23, tb_wid=378,
                  tb_ht=334)
    kwargs['tint'] = 0.14*(0.75*kwargs['wsp'] + 5.6) / kwargs['wsp']

    # step wind
    step_path = mod_dir + f'htc/{basename}_step.htc'
    base_to_step(htc_base, step_path, **kwargs)

    # turb wind
    turb_path = mod_dir + f'htc/{basename}_turb.htc'
    base_to_turb(htc_base, turb_path, **kwargs)

    # ======= clone dll repo and copy in the ones we need =======
    control_dir = mod_dir + 'control/'  # must end with slash!

    # delete dlls in control repo
    try:  # if control folder exists, delete dlls
        delete_dlls(control_dir)
    except FileNotFoundError:  # if control folder doesn't exist, create it
        os.mkdir(control_dir)

    # clone dll repo (will throw error if repo not deleted)
    clone_repo(dll_git_url, dll_branch)

    # copy dlls to control repo
    [shutil.copy('control-win32/' + t[0], control_dir + t[1])
      for t in dll_list]

    # copy in the wire ESYS dll
    try:  # try to delete wire directory if it exists
        shutil.rmtree(mod_dir + 'wire/')
    except FileNotFoundError:  # if doesn't exist, make it and copy in dll
        os.mkdir(mod_dir + 'wire/')
        shutil.copy('control-win32/ESYSMooring_trirotor.dll',
                    mod_dir + 'wire/ESYSMooring.dll')

    # delete control binaries
    try:
        shutil.rmtree('./control-win32/')
    except PermissionError:  # doesn't work on WINDOWS (local or CPAV) >:(
        pass
    #subprocess.run(['rmdir', '/s', '/q', 'control-win32'], shell=True, check=True)

    # ======= write the git branch and commit to file =======
    url, branch, commit, date = get_repo_info()
    with open(mod_dir + 'git_version.txt', 'w') as f:
        f.write(f'Git info for {basename} zip file\n')
        f.write('------------------------------------\n')
        f.write('Automatically generated using CI on following repo.\n\n')
        f.write(f'Repo url: {url}\n')
        f.write(f'Branch: {branch}\n')
        f.write(f'Commit: {commit} made on {date}\n')

    # ======= make the archive =======
    shutil.make_archive(mod_dir, 'zip', zip_name)
