\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{risoe_I_new}[2006/10/25 - Risoe report document class]

\DeclareOption{a5paper}
    {\ClassError{risoe}{Option `a5paper' not supported}{}}
\DeclareOption{b5paper}
    {\ClassError{risoe}{Option `b5paper' not supported}{}}
\DeclareOption{letterpaper}
    {\ClassError{risoe}{Option `letterpaper' not supported}{}}
\DeclareOption{legalpaper}
    {\ClassError{risoe}{Option `legalpaper' not supported}{}}
\DeclareOption{executivepaper}
    {\ClassError{risoe}{Option `executivepaper' not supported}{}}
\DeclareOption{landscape}
    {\ClassError{risoe}{Option `landscape' not supported}{}}
\DeclareOption{11pt}
    {\ClassError{risoe}{Option `11pt' not supported}{}}
\DeclareOption{12pt}
    {\ClassError{risoe}{Option `12pt' not supported}{}}
\DeclareOption{oneside}
    {\ClassError{risoe}{Option `oneside' not supported}{}}
\DeclareOption{twocolumn}
    {\ClassError{risoe}{Option `twocolumn' not supported}{}}
\DeclareOption{titlepage}
    {\ClassError{risoe}{Option `titlepage' not supported}{}}
\DeclareOption{notitlepage}
    {\ClassError{risoe}{Option `notitlepage' not supported}{}}
\DeclareOption{leqno}
    {\ClassError{risoe}{Option `leqno' not supported}{}}
\DeclareOption{dk}{\AtEndOfClass{\input{dk.clo}}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass[a4paper,10pt,twoside,onecolumn]{article}

\renewcommand{\normalsize}{\@setsize\normalsize{12.0dd}\xpt\@xpt
  \abovedisplayskip 6dd plus2dd minus4dd\belowdisplayskip \abovedisplayskip
  \abovedisplayshortskip \z@ plus3dd\belowdisplayshortskip 6dd plus 3dd minus 3dd\let\@listi\@listI}
\def\small{\@setsize\small{10dd}\viiipt\@viiipt
  \abovedisplayskip 8.5pt plus 3pt minus 4pt\belowdisplayskip \abovedisplayskip
  \abovedisplayshortskip \z@ plus2pt\belowdisplayshortskip 4pt plus2pt minus 2pt
  \def\@listi{\topsep 4pt plus 2pt minus 2pt\parsep 2pt plus 1pt minus 1pt
  \itemsep \parsep}}
\def\footnotesize{\@setsize\footnotesize{9.5pt}\viiipt\@viiipt
  \abovedisplayskip 6pt plus 2pt minus 4pt\belowdisplayskip \abovedisplayskip
  \abovedisplayshortskip \z@ plus 1pt
  \belowdisplayshortskip 3pt plus 1pt minus 2pt
\def\@listi{\topsep 3pt plus 1pt minus 1pt\parsep 2pt plus 1pt minus 1pt \itemsep \parsep}}
\def\scriptsize{\@setsize\scriptsize{8pt}\viipt\@viipt}
\def\tiny{\@setsize\tiny{6pt}\vpt\@vpt}
\def\large{\@setsize\large{14pt}\xiipt\@xiipt}
\def\Large{\@setsize\Large{16dd}\xivpt\@xivpt}
\def\LARGE{\@setsize\LARGE{22pt}\xviipt\@xviipt}
\def\huge{\@setsize\huge{20dd}\xxpt\@xxpt}
\def\Huge{\@setsize\Huge{30pt}\xxvpt\@xxvpt}
\normalsize

\if@twoside \oddsidemargin 14.6mm \evensidemargin 12.1mm \marginparwidth 37.5mm
      \else \oddsidemargin 14.6mm \evensidemargin 12.1mm \marginparwidth 37.5mm
\fi
\marginparsep 0mm
\topmargin -5.4mm
\headheight 0mm
\headsep 0mm
\footskip 24dd
\textheight = 247mm
\textwidth 132.5mm \columnsep 10pt \columnseprule 0pt

\footnotesep 6.65pt
\skip\footins 9pt plus 4pt minus 2pt
\floatsep 12dd plus 2dd minus 2dd
\textfloatsep 12dd plus 2dd minus 2dd
\intextsep 12dd plus 2dd minus 2dd
\dblfloatsep 12pt plus 2pt minus 2pt
\dbltextfloatsep 20pt plus 2pt minus 4pt
\@fptop 0pt plus 1fil \@fpsep 8pt plus 2fil \@fpbot 0pt plus 1fil
\@dblfptop 0pt plus 1fil \@dblfpsep 8pt plus 2fil \@dblfpbot 0pt plus 1fil
\marginparpush 5pt

\parskip 6dd plus 1dd \parindent 0em \partopsep 0dd plus 1dd minus 1dd
\@lowpenalty 51 \@medpenalty 151 \@highpenalty 301
\@beginparpenalty -\@lowpenalty \@endparpenalty -\@lowpenalty
\@itempenalty -\@lowpenalty

\def\section{\@startsection {section}{1}{\z@}{-22dd plus 0ex minus 0ex}{14dd plus 0ex}{\LARGE \bfseries \sffamily}}
\def\subsection{\@startsection{subsection}{2}{\z@}{-14dd plus 0ex minus 0ex}{4dd plus 0ex}{\large\bfseries \sffamily}}
\def\subsubsection{\@startsection{subsubsection}{3}{\z@}{-18dd plus 0ex minus 0ex}{2dd plus 0ex}{\normalsize\bfseries \sffamily}}
\def\paragraph{\@startsection{paragraph}{4}{\z@}{12dd plus 0ex minus 0ex}{-1em}{\normalsize\bfseries \sffamily}}
\def\subparagraph{\@startsection{subparagraph}{4}{\parindent}{3.25ex plus 1ex minus .2ex}{-1em}{\normalsize\bfseries}}

\setcounter{secnumdepth}{3}

\def\appendix{\par
 \addtocontents{toc}{\protect\setcounter{tocdepth}{1}}
 \setcounter{section}{0}
 \setcounter{subsection}{0}
 \def\thesection{\Alph{section}}
 \def\theequation{\Alph{section}.\arabic{equation}}}

\leftmargini 2em \leftmarginii 22pt \leftmarginiii 18.7pt
\leftmarginiv 17pt \leftmarginv 10pt \leftmarginvi 10pt
\leftmargin\leftmargini
\def\@listI{\leftmargin\leftmargini \parsep 0dd plus 2dd minus 1dd
                                    \topsep 6dd plus 2dd minus 4dd
                                    \itemsep 6dd plus 2dd minus 1dd}
\let\@listi\@listI

\relax

\setcounter{tocdepth}{2}
%
% Definition of \tableofcontents
%
\def\@dottedtocline#1#2#3#4#5{\ifnum #1>\c@tocdepth \else
  \vskip \z@ plus .2pt
  {\hangindent #2\relax \rightskip \@tocrmarg \parfillskip -\rightskip
    \parindent #2\relax\@afterindenttrue
   \interlinepenalty\@M
   \leavevmode
   \@tempdima #3\relax
    #4\nobreak\hbox{$\mbox{}\quad\mbox{}$} \nobreak \hbox
    to\@pnumwidth{\em #5\hfil}\hfill\mbox{}\par}\fi}

\def\tableofcontents{\section*{Contents\@mkboth{CONTENTS}{CONTENTS}}
 \vskip 18dd\@starttoc{toc}}
\def\l@part#1#2{
  \addpenalty{\@secpenalty}
  \addvspace{2.25em plus 1pt}
  \begingroup
    \@tempdima 3em
    \parindent \z@
    \rightskip \@pnumwidth
    \parfillskip -\@pnumwidth
   {\large \bf \leavevmode #1\box{} \quad {#2}\hfil \mbox{}}\par
    \nobreak
  \endgroup}
\def\l@section#1#2{
  \addpenalty{\@secpenalty}
  \addvspace{12dd plus 1dd} \@tempdima 1.5em
  \begingroup
    \parindent \z@
    \rightskip \@pnumwidth
    \parfillskip -\@pnumwidth
    \bf \leavevmode #1\it \mbox{} \quad {#2}\hfil \mbox{} \par
 \endgroup}


\setcounter{topnumber}{2}
\def\topfraction{0.95}
\setcounter{bottomnumber}{2}
\def\bottomfraction{0.95}
\setcounter{totalnumber}{4}
\def\textfraction{.05}
\def\floatpagefraction{.9}
\setcounter{dbltopnumber}{2}
\def\dbltopfraction{.9}
\def\dblfloatpagefraction{.8}

\long\def\@makecaption#1#2{
 \vskip 12dd
 \setbox\@tempboxa\hbox{#1. #2}
 \ifdim \wd\@tempboxa >\hsize #1. #2\par \else \hbox
 to\hsize{\box\@tempboxa\hfil}
 \vskip 12dd
 \fi}

\def\thefigure{\@arabic\c@figure}
\def\fps@figure{htbp}
\def\ftype@figure{1}
\def\ext@figure{lof}
\def\fnum@figure{\em Figure \thefigure}
\def\figure{\@float{figure}}
\def\endfigure{\end@float}
\@namedef{figure*}{\@dblfloat{figure}}
\@namedef{endfigure*}{\end@dblfloat}
\def\thetable{\@arabic\c@table}
\def\fps@table{htbp}
\def\ftype@table{2}
\def\ext@table{lot}
\def\fnum@table{\em Table \thetable}
\def\table{\@float{table}}
\def\endtable{\end@float}
\@namedef{table*}{\@dblfloat{table}}
\@namedef{endtable*}{\end@dblfloat}

\def\report#1{\def\reportnum{#1}}
\def\reportnum{}
\def\code#1{\def\reportlan{#1}}
\def\reportlan{}
\def\abstractheading#1{\def\abstracthead{#1}}
\def\abstracthead{Abstract}

\def\maketitle{
  \begingroup
    \def\thefootnote{\fnsymbol{footnote}}
    \def\@makefnmark{\hbox to 0pt{$^{\@thefnmark}$\hss}}
    \global\@topnum\z@ \@maketitle
    \thispagestyle{lempty}
    \@thanks
  \endgroup
  \setcounter{footnote}{0}
  \let\maketitle\relax
  \let\@maketitle\relax
  \gdef\@thanks{}
  \gdef\@author{}
  \gdef\@title{}
  \let\thanks\relax}
\def\@maketitle{
  \setlength{\unitlength}{1mm}
  \begin{picture}(210,100)(0,0)
%%\put(-2,-197){\mbox{  {!}{\includegraphics{eps/template0_new.eps}}}}
  \put(125,75){\mbox{\resizebox{58mm}{!}{\includegraphics{eps/template1_new.eps}}}}
  \put(55.4,0){\makebox(117.2,79.5)[t]{\parbox{117.2mm}{
    \begin{flushright}  \vskip 40mm
    {\huge  \sffamily \@title} \vskip 10mm
    {\large \sffamily \@author}
    {\large \sffamily \mbox{\reportnum \reportlan}} \vskip 5mm
    \end{flushright}}}}
  \put(178,-183){\mbox{\resizebox{15mm}  {!}{\includegraphics{eps/template2_new.eps}}}}
  \put(84,-183){\makebox(88.4,22)[t]{\parbox{88.4mm}{\large
    \begin{flushright} \sffamily
                     Ris{\o} National Laboratory for Sustainable Energy \\
                     Technical University of Denmark \\
                     Roskilde, Denmark \\
 \ifcase\month\or
 January\or February\or March\or April\or May\or June\or
 July\or August\or September\or October\or November\or December\fi
 \space\number\year
    \end{flushright}}}}
 \end{picture}
}

\renewenvironment{abstract}{\setcounter{page}{2}
\thispagestyle{empty} \paragraph{\abstracthead}\mbox{}}{\vfill}

\mark{{}{}}

\newcommand{\marginnote}[1]{\@mparswitchfalse
 \marginpar{\em\raggedright #1\mbox{}\\}}

\def\ps@headings{\let\@mkboth\markboth
\def\@oddfoot{\rm\sffamily\reportnum \hfill \sffamily\thepage}
\def\@evenfoot{\rm\sffamily\thepage \hfill \sffamily\reportnum}
\def\@evenhead{}
\def\@oddhead{}
\def\sectionmark##1{\markboth {\uppercase{\ifnum \c@secnumdepth
>\z@
 \thesection\hskip 1em\relax \fi ##1}}{}}\def\subsectionmark##1{\markright
{\ifnum \c@secnumdepth >\@ne \thesubsection\hskip 1em\relax \fi ##1}}}
\def\ps@ll{\renewcommand{\marginnote}[1]{\@mparswitchfalse
 \marginpar{\em\raggedright ##1\\}}
 \oddsidemargin 14.6mm \evensidemargin 14.6mm}
\def\ps@cc{\renewcommand{\marginnote}[1]{\@mparswitchtrue
 \marginpar[\em\raggedleft ##1\\]{\em\raggedright ##1\\}}
 \oddsidemargin 14.6mm \evensidemargin 14.6mm} % 2em ekstra
\def\ps@rl{\renewcommand{\marginnote}[1]{\@mparswitchtrue
 \marginpar[\em\raggedleft ##1\\]{\em\raggedright ##1\\}}
 \oddsidemargin 14.6mm \evensidemargin 14.6mm}
\def\ps@rr{\renewcommand{\marginnote}[1]{\@mparswitchfalse
 \reversemarginpar\marginpar{\em\raggedleft ##1\\}}
 \oddsidemargin 14.6mm \evensidemargin 14.6mm}
\def\ps@lempty{\let\@mkboth\@gobbletwo
 \def\@oddhead{} \def\@oddfoot{}
 \def\@evenhead{}\def\@evenfoot{}
 \oddsidemargin -25.4mm \topmargin -25.4mm \footskip 0mm}

\ps@headings \pagenumbering{arabic} \onecolumn \if@twoside\else\raggedbottom\fi
%
%   Other special command and environments
%
\newlength{\tabularindent}
\tabularindent = 2em
\addtolength{\tabularindent}{-\tabcolsep}
\newcommand{\indenttext}{\noindent\hspace*{\tabularindent}}
\newcommand{\bigpar}{\vskip \baselineskip \noindent}

\newenvironment{colophon}{\vfill\parskip=\baselineskip\parindent=0dd}{
 \thispagestyle{empty}\clearpage\parskip=0dd\parindent=1em}{\clearpage}

\newcommand{\oneblankpage}{\clearpage\mbox{}\thispagestyle{empty}\clearpage}
\newcommand{\skiponepage}{\clearpage\mbox{}\clearpage}
\newcommand{\standardsection}[1]{\section*{#1}
 \addcontentsline{toc}{section}{\protect\numberline{}{#1}}}

\newlength{\figureheight}
\newcommand{\rfigure}[2]{\setlength{\fboxrule}{0.0pt}
 \setlength{\figureheight}{#1}\addtolength{\figureheight}{-4dd}
 \noindent\framebox[#2]{\rule[0dd]{0dd}{\figureheight}}\setlength{\fboxrule}{0.4pt}}
\newcommand{\rframe}[2]{\setlength{\fboxrule}{0.4pt}
 \setlength{\figureheight}{#1}\addtolength{\figureheight}{-5.2dd}
 \noindent\framebox[#2]{\rule[0dd]{0dd}{\figureheight}}\setlength{\fboxrule}{0.4pt}}
\newcommand{\specialframe}[3]{
 \setlength{\figureheight}{#1}\addtolength{\figureheight}{-6dd}
 \noindent\framebox[#2]{\rule[0dd]{0dd}{\figureheight}
 }}

%\def\dato{\number\year--\number\month--\number\day}   % (1987-12-24)
\def\idag{\number\day. \ifcase\month\or
 januar\or februar\or marts\or april\or maj\or juni\or
 juli\or august\or september\or oktober\or november\or december\fi
 , \number\year}   % (24. december, 1987)
%
%   Other useful commands
%
\newcommand{\dg}{\mbox{$^{\circ}$}}              % grader
\newcommand{\mn}{\mbox{$'$}}                     % minutter
\newcommand{\sk}{\mbox{$''$}}                    % sekunder

\newcommand{\etal}{{\em et al.}}                 % et al. i kursiv

\newcommand{\ms}{\mbox{$\rm m\,s^{-1}$}}         % meter per sekund
\newcommand{\wsqm}{\mbox{$\rm Wm^{-2}$}}         % Watt per kvadratmeter
%
%   New math mode commands
%
\newcommand{\arccot}{\mathop{\mathrm{arccot}}\nolimits}
\newcommand{\cosec}{\mathop{\mathrm{cosec}}\nolimits}
\newcommand{\erf}{\mathop{\mathrm{erf}}\nolimits}
\newcommand{\re}{\mathop{\mathrm{Re}}\nolimits}
\newcommand{\im}{\mathop{\mathrm{Im}}\nolimits}
\newcommand{\tr}{\mathop{\mathrm{tr}}\nolimits}
\newcommand{\ei}{\mathop{\mathrm{Ei}}\nolimits}
\newcommand{\sgn}{\mathop{\mathrm{sgn}}\nolimits}
\newcommand{\ent}{\mathop{\mathrm{ent}}\nolimits}
\newcommand{\grad}{\mathop{\mathbf{grad}}\nolimits}
\newcommand{\rot}{\mathop{\mathbf{rot}}\nolimits}
\newcommand{\curl}{\mathop{\mathbf{curl}}\nolimits}
\mathchardef\dividedby="2204
\renewcommand{\div}{\mathop{\mathrm{div}}\nolimits} % \div redefined!
\def\corrto{\mbox{\tabcolsep 2pt
 \begin{tabular}{c}\huge\symbol{'136}\\[-4.3ex]$=$\end{tabular}}}

\newcommand{\rd}[1]{\mathrm{d}\mathit{#1}}
\newcommand{\rD}[1]{\mathrm{D}\mathit{#1}}
\newcommand{\vektor}[1]{\mbox{\boldmath$#1$\unboldmath}}
\newcommand{\mbf}[1]{\mbox{\boldmath$#1$\unboldmath}}
\newcommand{\tensor}[1]{\mbox{\sffamily\slshape #1}}
\newcommand{\msf}[1]{\mbox{\sffamily\slshape #1}}
\newcommand{\ds}{\displaystyle}                  % displaystyle
\newcommand{\ts}{\textstyle}                     % textstyle
%
%   References: \begin{references}...\end{references}
%   Use \filbreak between each reference if they should not be
%   broken across pages
%
\newcommand{\hvertafsnit}{\everypar}
\newcommand{\refhang}{\hangindent=1.5em\hangafter 1}
\newenvironment{references}{\parindent=0pt \parskip=0pt
\par\frenchspacing\begin{sloppypar}\everypar={\refhang}}%
{\everypar={\hvertafsnit}\parskip=0pt\end{sloppypar}\par\mbox{}}

\newcommand{\thtybf}{\fontsize{29.86}{30dd}\normalfont\bfseries}

\newcount\TestCount
\def\La{\TestCount=\the\fam \leavevmode L\raise.42ex
         \hbox{$\fam\TestCount\scriptstyle\kern-.3em A$}}

\def\LaTeX{\La\kern-.15em\TeX}

\def\AllTeX{{(\La)\TeX}}

\def\AmSTeX{{\the\textfont2 A}\kern-.1667em\lower.5ex\hbox
        {\the\textfont2 M}\kern-.125em{\the\textfont2 S}-\TeX}

\DeclareFontShape{OT1}{cmr}{m}{n}
   {  <5> <6> <7> <8> <9> <10> <12> gen * cmr
      <10.95> cmr10
      <14.4>  cmr12
      <17.28><20.74><24.88><29.86>cmr17}{}

\DeclareFontShape{OT1}{cmr}{bx}{n}
   {
      <5> <6> <7> <8> <9> gen * cmbx
      <10><10.95> cmbx10
      <12><14.4><17.28><20.74><24.88><29.86>cmbx12
      }{}
%
%   Bibliographical data sheet - commands in Danish
%
\newcommand{\titel}[1]{{\footnotesize Title and author(s)}
 \rm\\[3dd]#1\\[\baselineskip]}

\newcommand{\forfatter}[1]{\footnotesize{{\bf Author:} #1}\mbox{}}

\newcommand{\isbn}[1]{{\footnotesize {\bf ISBN #1}}\mbox{}}

\newcommand{\issn}[1]{{\footnotesize {\bf ISSN #1}}\mbox{}}

\newcommand{\reportnr}[1]{{\footnotesize {\bf #1}}\mbox{}}

\newcommand{\nopages}[1]{{\footnotesize {\bf Pages: #1}}\mbox{}}

\newcommand{\notables}[1]{{\footnotesize {\bf Tables: #1}}\mbox{}}

\newcommand{\norefs}[1]{{\footnotesize {\bf References: #1}}\mbox{}}

\newcommand{\dato}[1]{{\footnotesize {\bf #1}}\mbox{}}

\newcommand{\kontraktnr}[1]{{\footnotesize {\bf Contract no.:} \newline #1}\mbox{}}

\newcommand{\pspnr}[1]{{\footnotesize {\bf Group's own reg. no.:} \newline #1}\mbox{}}

\newcommand{\sponsor}[1]{{\footnotesize {\bf Sponsorship:} \newline #1}\mbox{}}

\newcommand{\cover}[1]{{\footnotesize {\bf Cover:} \newline #1}\mbox{}}

\newcommand{\afdeling}[1]{{\footnotesize {\bf Department:} #1}\mbox{}}

\newcommand{\overskrift}[1]{{\footnotesize {\bf Title:} #1}\mbox{}}

\newcommand{\infoservices}[1]{{\footnotesize  Information Service Department \newline
Ris� National Laboratory for \newline Sustainable Energy\newline Technical University of Denmark \newline
P.O.Box 49 \newline DK-4000 Roskilde \newline Denmark \newline Telephone +45 4677 4005
\newline bibl@risoe.dtu.dk \newline Fax +45 4677 4013 \newline www.risoe.dtu.dk}\mbox{}}


\newcommand{\regnummer}[1]{\parbox[t]{0.5\textwidth}{{\footnotesize
 Groups own reg. number(s)}\rm\\[3dd] #1\mbox{}}}

\newcommand{\projektnummer}[1]{\parbox[t]{0.5\textwidth}{{\footnotesize
 Project/contract No.}\rm\\[3dd] #1\mbox{}}\\[0.5\baselineskip]
 \underline{\makebox[\textwidth]{\mbox{}}}\\[-3dd]}

\newcommand{\sider}[1]{\parbox[t]{0.25\textwidth}{{\footnotesize Pages}
 \rm\\[3dd]\mbox{}#1\mbox{}}}

\newcommand{\tabeller}[1]{\parbox[t]{0.25\textwidth}{{\footnotesize Tables}
 \rm\\[3dd]\mbox{}#1\mbox{}}}

\newcommand{\figurer}[1]{\parbox[t]{0.25\textwidth}{{\footnotesize
 Illustrations}\rm\\[3dd]\mbox{}#1\mbox{}}}

\newcommand{\referencer}[1]{\parbox[t]{0.25\textwidth}{{\footnotesize
 References}\rm\\[3dd]\mbox{}#1\mbox{}}\\[0.5\baselineskip]
 \underline{\makebox[\textwidth]{\mbox{}}}\\[-3dd]}

\newcommand{\resume}[1]{{\footnotesize Abstract (Max. 2000 char.)}
 \rm\\[3dd]#1\mbox{}\\\underline{\makebox[\textwidth]{\mbox{}}}\\[-3dd]}

\newcommand{\deskriptorer}[1]{{\footnotesize Descriptors INIS/EDB}
 \rm\\[3dd]#1\mbox{}\\
 \underline{\makebox[\textwidth]{\mbox{}}}\\[-3dd]}

\newenvironment{datablad}{\parindent 0pt\parskip 0pt\clearpage
 \frenchspacing\thispagestyle{empty}\normalsize
 \underline{\makebox[\textwidth]{\bf Bibliographic Data Sheet
 \rule[-6dd]{0cc}{1cc}\hfill\reportnum \reportlan}}\\}{
\clearpage}
%
%   Invisible digits in vertical mode (tables and math)
%
\newdimen\digitwidth  % usynligt tal
\setbox0=\hbox{\rm0}
\digitwidth=\wd0
\newdimen\periodwidth % usynligt punktum
\setbox1=\hbox{\rm.}
\periodwidth=\wd1
\newdimen\commawidth  % usynligt komma
\setbox2=\hbox{\rm,}
\commawidth=\wd2
\newdimen\smalldigitwidth  % usynligt lille tal
\setbox3=\hbox{\footnotesize 0}
\smalldigitwidth=\wd3
%
%   Default values
%
\frenchspacing \raggedbottom \jot=6dd \language=0
%
% new definition of parmargin
%                               thbu 03-2008
\setlength{\marginparwidth}{2cm}
\let\oldmarginpar\marginpar
\renewcommand\marginpar[1]{\-\oldmarginpar[\raggedleft\footnotesize {\hspace{-1cm}\parbox{\marginparwidth}{#1}\hspace*{1cm}}]%
{\raggedright\footnotesize {\hspace{1cm}\parbox{\marginparwidth}{#1}\hspace*{-1cm}}}}

