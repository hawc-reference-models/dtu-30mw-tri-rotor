
% ----------------------------------------------------------------
\section{Introduction}
\label{sec:intro}

This archive contains a HAWC2 implementation of the DTU 30 MW Tri-Rotor.
The DTU 30 MW Tri-Rotor is a fictitious, multi-rotor horizontal axis wind turbine model that was created by DTU to demonstrate HAWC2's modeling flexibility.
The model features three rotor nacelle assemblies (RNAs), each of which has the same specifications as the DTU 10 MW reference wind turbine RNA \cite{Bak2013}.
The center RNA sits on directly on the tower, and the left and right RNAs rest on two arms that extend horizontally out from the tower.
Guy wires are also added for increased stability.
Because this is a fictitious model meant to demonstrate certain capabilities, no detailed aeroelastic design or analysis has been performed.
A diagram of the tri-rotor is given in Fig.~\ref{fig:trirotor}.

\begin{figure}
\centering
\includegraphics[width=3in]{tri-rotor-vis.png}
\caption{Visualisation of the DTU 30 MW Tri-Rotor.}
\label{fig:trirotor}
\end{figure}

% ----------------------------------------------------------------
\section{Model history and versioning}

The model has undergone relatively few changes since being first developed by Christos Galinos.
A description of the model versions can be found in Table~\ref{tab:versions}.

\begin{table}\centering
	\caption{Summary of model versions}
	\begin{tabular}{c|p{4in}}\hline
	\textbf{Version} & \textbf{Notes/Changes to HAWC2 files} \\\hline
	1.0 & Original files distributed on \url{www.hawc2.dk} \\\hline
	1.1 & Changes to file structure and comments.
		No changes to model parameters.
	\end{tabular}
	\label{tab:versions}
\end{table}

% ----------------------------------------------------------------
\section{Model structure}

As discussed in Sec.~\ref{sec:intro}, the three RNAs are identical copies of the DTU 10 MW.
The tower has been modified to be 260~m long.
The two bottom rotors have a hub height of 108~m above ground, and the top rotor has a hub height of 263~m above ground.
Each arm is 100~m long.
Three guy wires connect the tower to the ground for stability.
Additionally, a single guy wire connects each arm to the tower for stability.
The rotors are controlled independently using three instances of the DTU Basic Controller \cite{Hansen2013}.
At rated wind speed, each RNA produces 10 MW, yielding a net production of 30 MW.
A summary of important parameters is given in Table~\ref{tab:parms}.

\begin{table}\centering
\caption{Parameters for DTU 10 MW Tri-Rotor}
\begin{tabular}{l|l|l} \hline
\textbf{Property} & \textbf{Unit} & \textbf{Value} \\\hline
Number of rotors & [-] & 3 \\
Rotor diameter & [m] & 178.332 \\
Rotor rated electrical power & [kW] & 10000 \\
Total WT rated electrical power & [kW] & 30000 \\
Number of blades & [-] & 9 \\
Hub-height upper rotor & [m] & 263 \\
Hub-height lower rotors & [m] & 108 \\
Blade length & [m] & 86.4 \\
Number of generators & [-] & 3 \\
Generator efficiency & [-] & 0.95 \\
Minimum rotor speed & [rad/s] & 0.628 \\
 &[rpm] & 5.997 \\
 &[Hz] & 0.100 \\
Rated rotor speed & [rad/s] & 1.005 \\
 &[rpm] & 9.597 \\
 & [Hz] & 0.160 \\
Cut-in wind speed & [m/s] & 3 \\
Cut-out wind speed & [m/s] & 25 \\
Maximum blade pitch angle rate & [deg/s] & 4 \\
Number of controllers & [-] & 3 \\
Controller name & [-] & DTU Basic Controller \\
Above rated control & [-] & Constant power \\\hline
\end{tabular}
\label{tab:parms}
\end{table}

% ----------------------------------------------------------------
\section{Repository structure}

This repo uses a branch structure to track the different model versions.
For example, version 1.0 of the model can be found in branch \texttt{v1.0}.
These branches are locked to prevent unauthorized pushes or merges.
Updates to comments and/or line order are allowed within a released version.
However, any changes to model parameters or outputs are not allowed.
If a user requires changes to a released version, they must do so on a separate branch, and these changes cannot be merged.
Updates to the model itself (e.g., fixing bugs or improving the controller) must be submitted as a merge request on the master branch and then reviewed/accepted by the repo responsible.

This repo contains the source code for this report, some utilities for model generation, and the HAWC2 input files for the model.
The folder structure for the input files is as shown:
\begin{verbatim}
- control/  <-- control DLLs go here
   - wpdata.100  <-- text files for DTU Basic Controller
- data/  <-- files for bodies and aeroparameters
- htc/  <-- HAWC2 files
   - dtu_30mw_tri_rotor.htc  <-- 100-s steady simulation, 8 m/s
\end{verbatim}

The repo also features CI/CD pipelines to compile a zip file containing this report and a collection of HAWC2 files.
In particular, the additional HAWC2 files that are created are a 600-s turbulent simulation and a step-wind simulation.
Should a user need to regenerate these HAWC2 files locally, they may look into the scripts in the \texttt{utils/} folder.
Note that these scripts have dependencies that must be satisfied in order to run.
Alternatively, the most recent build of the zip file can be downloaded by clicking this link:

\url{https://gitlab.windenergy.dtu.dk/hawc-reference-models/dtu-30mw-tri-rotor/-/jobs/artifacts/master/raw/dtu-30mw-tri-rotor.zip?job=make_32bit_zip}.

If available, the zip files corresponding to specific versions may be downloaded by replacing the word ``master'' in the link above with the respective branch name (e.g., ``v1.1'').


% ----------------------------------------------------------------
\section{Bugs, model improvements and questions}

If you as a user notice that there is a bug in the model (e.g., a sign error or a parameter that doesn't match the model documentation), then we kindly ask that you notify us by submitting an issue on GitLab.
Navigate to the repo on GitLab, then click on ``Issues'' in the side bar.
The Issue Tracker on GitLab is how we keep track of issues with the model and things that should be fixed for the next model release.

If you have questions about a model that are not answered by this document, then please email \href{mailto:hawc2@windenergy.dtu.dk}{hawc2@windenergy.dtu.dk}.
