# dtu-30mw-tri-rotor

HAWC2 files for the DTU 30 MW Tri-Rotor Model.

A zip file containing the most recent model files and PDF report can be
downloaded by
[clicking this link](https://gitlab.windenergy.dtu.dk/hawc-reference-models/dtu-30mw-tri-rotor/-/jobs/artifacts/master/raw/dtu-30mw-tri-rotor.zip?job=make_32bit_zip).